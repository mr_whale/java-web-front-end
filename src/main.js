// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vant from 'vant';
import axios from "axios";
import 'vant/lib/index.css';
import './assets/delete.css';
import VueAxios from 'vue-axios'
import service from './services'
import VueTouch from 'vue-touch'
import VueClipboard from 'vue-clipboard2'
import QRCode from 'qrcodejs2'


Vue.use(VueClipboard)
Vue.use(VueTouch, {name: 'v-touch'})
Vue.use(service)
Vue.use(VueAxios, axios);
Vue.use(Vant);
Vue.use(router)
axios.defaults.headers.post['Content-Type']='text/plain'
Vue.prototype.$axios=axios
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  // 得到本地存储的token  想要进其他页面，就要带token
  let token = localStorage.getItem('token')
  if (token) {
    if (to.path === '/Log') {
      next({ path: '/' });
    } else {
      next();
    }
  } else {
    if (to.path !== '/Log') {
      next({ path: '/Log' });
    } else {
      next();
    }
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
