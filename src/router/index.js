import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/index'
import Upload from '@/pages/upload'
import Getphoto from '@/pages/getphoto'
import Log from '@/pages/login'
import Photo from '@/pages/photo'
import Album from '@/pages/album'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Log',
      name: 'Log',
      component: Log
    },
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload
    },
    {
      path: '/getphoto',
      name: 'Getphoto',
      component: Getphoto
    },
    {
      path: '/photo',
      name: 'Photo',
      component: Photo
    },
    {
      path: '/album',
      name: 'Album',
      component: Album
    },
  ]
})
